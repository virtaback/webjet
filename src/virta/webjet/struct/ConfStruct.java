package virta.webjet.struct;

import java.util.HashMap;

/**
 * Describes Structure of Config
 * 
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 * @version 0.5
 *
 */
public final class ConfStruct {
	private static ConfStruct self;
	
	private String context;
	private String mainPkg;
	private HashMap<String,String> servlets;
	
	/**
	 * Returns config struct
	 * 
	 * @param context path for current Handler
	 * @param mainPkg main package where App can find servlets
	 * @return ConfStruct struct  
	 */
	public static ConfStruct getConfig(String context, String mainPkg){
		if(self==null){
			self = new ConfStruct(context, mainPkg);
		}
		return self;
	}
	
	/**
	 * Constructor without servlets
	 * 
	 * @param context Context (Path from site root for this site). @see getContext 
	 * @param mainPkg main package, where Servlets are located
	 */
	private ConfStruct(String context,String mainPkg){
		this.context=context;
		this.mainPkg=mainPkg;
	}
	/**
	 * Returns Context (Path from site root for this site)
	 * Note: Internal use in normal case 
	 * @return Context path of this handler
	 */
	public String getContext() {
		return context;
	}
	/**
	 * Sets Context (Path from site root for this site)<BR>
	 * Example: your Jet Site located at http://site/app (assume that http://site is served by nginx or other web server), 
	 * then context will "/app"
	 * 
	 * @param context Context to set
	 */
	public void setContext(String context) {
		this.context = context;
	}
	
	
	/**
	 * Returns mainPkg
	 * Note: internal use normally.
	 * 
	 * @return the mainPkg
	 */
	public String getMainPkg() {
		return mainPkg;
	}
	
	/**
	 * Sets main package, where Servlets are located.
	 * Note: Currently we don't support default package
	 * 
	 * @param mainPkg Main Package to set
	 */
	public void setMainPkg(String mainPkg) {
		this.mainPkg = mainPkg;
	}
	
	/**
	 * Returns Servets
	 * Note: internal use only
	 * 
	 * @return Servlets HashMap
	 */
	public HashMap<String, String> getServlets() {
		return servlets;
	}
	/**
	 * Sets Servlets
	 * 
	 * @param servlets the servlets to set
	 */
	//TODO make it private
	public void setServlets(HashMap<String, String> servlets) {
		this.servlets = servlets;
	}
	/**
	 * Adds Servlet to HashMap
	 * 
	 * @param servletClass Class name (at this version only relative name allowed)
	 * @param path Path which this Servlet services
	 */
	public void addServlet(String servletClass,String path){
		//if no HashMap, we set it
		if(servlets==null){
			HashMap<String, String> srvlts = new HashMap<String,String>();
			this.setServlets(srvlts);
		}
		//since now we work with servlets private prop
		this.servlets.put(servletClass, path);
	}
}
