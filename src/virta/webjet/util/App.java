package virta.webjet.util;

import virta.webjet.logger.AppLogger;
import virta.webjet.logger.Level;
import virta.webjet.struct.ConfStruct;
/**
 * App Settings 
 * 
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 * @version 0.5
 *
 */
public class App {

	private static App self;
	
	
	//object fileds
	
	private App(){}
	
	private String accessLog;
	private String errorLog;
	private String pidFile;
	private String[] args;
	
	private ConfStruct config;
	
	private Throwable e;
	
	/**
	 * Return App (Settings objects)
	 * 
	 * @return Settings object (same every time)
	 */
	public static App getApp(){
		if(self==null){
			self = new App();
		}
		return self;
	}
	/**
	 * Reports current log level
	 *  
	 * @return log level
	 */
	public int getLogLevel(){
		return Level.AppLevel().getLevel();
	}
	/**
	 * Sets new log level
	 * 
	 * @param level
	 */
	public void setLogLevel(int level){
		Level.AppLevel().setLevel(level);
	}
	
	/**
	 * Returns logger
	 * @return logger
	 */
	public AppLogger getLogger(){
		return AppLogger.getLogger();
	}
	
	/**
	 * Gives access log file name
	 * 
	 * @return access log name or "shell"
	 */
	public String getAccessLog() {
		return accessLog;
	}
	
	/**
	 * Sets access.log setting
	 * 
	 * @param accessLog access log file name 
	 */
	public void setAccessLog(String accessLog) {
		this.accessLog = accessLog;
	}
	
	/**
	 * Gives error log file name
	 * 
	 * @return error log name or "shell"
	 */
	public String getErrorLog() {
		return errorLog;
	}
	
	/**
	 * Sets error.log setting
	 * 
	 * @param errorLog error log file name 
	 */
	public void setErrorLog(String errorLog) {
		this.errorLog = errorLog;
	}
	
	/**
	 * Gives pid file name
	 * 
	 * @return pid file name or "shell"
	 */
	public String getPidFile() {
		return pidFile;
	}

	/**
	 * Sets pid file setting
	 * 
	 * @param pidFile file name 
	 */
	public void setPidFile(String pidFile) {
		this.pidFile = pidFile;
	}
	/**
	 * Returns shell args. what were stored before
	 * 
	 * @return Shell args or null
	 */
	public String[] getArgs() {
		return args;
	}
	/**
	 * Stores Shell Args
	 * @param args Shell Arguments
	 */
	public void setArgs(String[] args) {
		this.args = args;
	}
	/**
	 * Returns Configuration
	 * 
	 * @return Struct with Config
	 */
	public ConfStruct getConfig() {
		return config;
	}
	/**
	 * Sets Configuration
	 * @param config Configuration as ConfStruct
	 */
	public void setConfig(ConfStruct config) {
		this.config = config;
	}
	/**
	 * Defines if any error occured at prev. stages or not
	 * 
	 * @return True - when no error was occured, False - elsewhere 
	 */
	public boolean isAppClean() {
		return (e == null);
	}
	/**
	 * Stores Exception (for isAppClean() usage)
	 * 
	 * @param e Exception
	 */
	public void setE(Throwable e) {
		if(e==null) return;
		
		this.e = e;
	}
}
