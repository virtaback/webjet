package virta.webjet.server;

import java.util.HashMap;
import java.util.Iterator;

import javax.naming.ConfigurationException;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import virta.sys.Env;
import virta.webjet.servlet.WebJetServlet;
import virta.webjet.struct.ConfStruct;
import virta.webjet.util.S;
/**
 * Internal Server Mechanisms
 * 
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 * @version 0.5
 *
 */
public class WebJetServer {
	/**
	 * Checks values
	 * 
	 * @param args re-translated arguments from command line
	 * @param config Server and servlets configuration as ConfStruct object 
	 * @throws ConfigurationException when provided configuration is wrong
	 * @throws IndexOutOfBoundsException when wrong number of args given
	 */
	public static void check(String[] args, ConfStruct config) throws ConfigurationException, IndexOutOfBoundsException{
		//args check to avoid NPE
		//args
		if(args==null){
			throw new ConfigurationException("No arguments were supplied. Cannot start server");
		}
		//context
		if(config==null){
			throw new ConfigurationException("No handler was created. Did you run setHandler() brefore start() ?");
		}
		
		if(args.length<1){
			throw new IndexOutOfBoundsException("Cannot start server. Reason: wrong number of arguments were send. Expected: 1-3. Got: "+args.length);
		}
		//checking port
		String rawPort = args[0];
		int intPort = Integer.parseInt(rawPort);
		if(intPort < S.MIN_PORT || intPort >= S.MAX_PORT){
			throw new NumberFormatException("Server Port must be a Number from 1 to 65535");
		}
		//TODO make this System-independent
		if(Env.getMyOS()=="unix"){
		if(intPort <= S.ROOT_PORT){
			//check for root
			
			String user = System.getProperty("user.name");
			if(!user.equals("root")){
				throw new NumberFormatException("You must be root to start server at ports less then 1024");
			}
		}
		}
		
		//Config checks
		String conText = config.getContext();
		if(conText == null || conText.equals("")){
			throw new ConfigurationException("Context in config cannot be empty. Use / instead");
		}
		//mainPkg
		String mainPkg = config.getMainPkg();
		if(mainPkg==null || mainPkg.equals("")){
			throw new ConfigurationException("Main package is not set or empty. Without this I cannot find your Servlets. In this version I cannot use default package.");
		}
		//Servlets
		HashMap<String, String> servlets = config.getServlets();
		if(servlets==null){
			throw new ConfigurationException("I cannot start without servlets. Please fix you configuration or start making your first servlet");
		}
	}
	
	
	/**
	 * Makes ready-to-use Jetty Handler
	 * 
	 * @param conf Configuration as ConfStruct
	 * @return ready-to-user Context Handler
	 * @throws ClassNotFoundException when App cannot find servlet class (possible reason here: wrong main package when adding handler)
	 * @throws ClassCastException when App cannot cast to WebJetServlet (normally this means that servlet class doesn't extend WebJetServlet class)
	 * @throws InstantiationException when class cannot be inited correctly (possible unaccessible constructor)
	 * @throws IllegalAccessException when class canoot be accesses (possible mess with access modifiers)
	 */
	public static ServletContextHandler configure(ConfStruct conf) throws ClassNotFoundException, ClassCastException, InstantiationException, IllegalAccessException{
		//make context
		ServletContextHandler context = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
		context.setContextPath(conf.getContext());
		
		//adding servlets
		HashMap<String,String> servlets = conf.getServlets();
		
		Iterator<String> iter = servlets.keySet().iterator();
		
		while(iter.hasNext()){
			String servletName = (String) iter.next();
			String path = servlets.get(servletName);
			
			//reflection
            Class<?> srvlt = Class.forName(conf.getMainPkg()+"."+servletName);
            WebJetServlet servlet = (WebJetServlet) srvlt.newInstance();
            
            context.addServlet(new ServletHolder(servlet),path);
		}
		
		return context;
	}
	/**
	 * Server starter: "custom port" edition
	 * 
	 * @param context handler (can be obtained by configure()) 
	 * @throws Exception when server not started
	 */
	public static void run(ServletContextHandler context) throws Exception{
		run(context,0);
	} 
	/**
	 * 
	 * Server starter 
	 * 
	 * @param context context handler (can be obtained by configure()) 
	 * @param port valid port
	 * @throws Exception when server not started
	 */
	public static void run(ServletContextHandler context,int port) throws Exception{
		//context -> server
		Server server = new Server(port);
		server.setHandler(context);
		
		//run
		server.start();
		server.join();

	} 
}
