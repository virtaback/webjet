package virta.webjet.server;

import java.io.IOException;

import javax.naming.ConfigurationException;

import org.eclipse.jetty.servlet.ServletContextHandler;

import virta.sys.Env;
import virta.sys.Shell;
import virta.webjet.logger.AppLogger;
import virta.webjet.logger.Level;
import virta.webjet.struct.ConfStruct;
import virta.webjet.util.App;
import virta.webjet.util.Lib;
/**
 * WebJet Server user interface
 * 
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 * @version 0.5
 *
 */
public class Server {
	/**
	 * Provides server object
	 * 
	 * @param args Arguments from command line
	 */
	public Server(String[] args){
		init(args);
	}
	/**
	 * Sets Handler
	 * Note: in this version we don't support multi-handler configuration. So in case method called twice last Handler will be used
	 * Example: server.setHandler("/","my.servlets")
	 *  
	 * @param context path which this Handler will serve
	 * @param mainPkg mainPackage where servlets are located (default package is still unsupported yet)
	 */
	public void setHandler(String context, String mainPkg){
		App.getApp().setConfig(ConfStruct.getConfig(context, mainPkg));
	}
	/**
	 * Adds Servlet to Handler
	 * Note: Handler must exist before method called
	 * 
	 * @param servletClass Servlet classname 
	 * @param path path which servlet will serve
	 */
	public void addServlet(String servletClass,String path){
		//init
		AppLogger log = App.getApp().getLogger();
		//take config
		ConfStruct config = App.getApp().getConfig();
		
		try{
		if(config==null){
			throw new Exception("Handler should exist before you can add servlets. Run setHandler() before addServlet()"); 
		}
		//addServlet
		config.addServlet(servletClass, path);
		}catch (Exception e) {
			app().setE(e);
			log.error(e.getMessage(),e);
			log.debug(e);
			return;
		}
	}
	
	/**
	 * Starts server
	 * 
	 */
	public void start(){
		//check if prev step were clean
		if(!app().isAppClean()) return;
		//init
		AppLogger log = App.getApp().getLogger();
		ServletContextHandler context = null;
		String[] args = App.getApp().getArgs();
		ConfStruct conf = App.getApp().getConfig();
		
		//check args
		//args
		if(args==null){
			log.error("No arguments were supplied. Cannot start server");
			return;
		}
		//context
		if(conf==null){
			log.error("No handler was created. Did you run setHandler() brefore start() ?");
			return;
		}
		
		log.info("Checking configuration");
		try{
			WebJetServer.check(args, conf);
		}catch(IndexOutOfBoundsException e){
			log.error(e.getMessage(), e);
			log.debug(e);
			return;
		}catch (ConfigurationException e) {
			log.error(e.getMessage(), e);
			log.debug(e);
			return;
		}
		log.info("Configure server");
		try {
			context = WebJetServer.configure(conf);
		} catch (ClassNotFoundException e) {
			log.error("Cannot find class with your servlet. Class: "+e.getMessage()+". This could be because of wrong class name, wrong mainPackage name or classpath issue", e);
			log.debug(e);
			return;
		} catch (ClassCastException e) {
			log.error("Cannot cast to WebJetServlet. Are you sure your servlet extends WebJetServlet ?", e);
			log.debug(e);
			return;
		} catch (InstantiationException e) {
			log.error(e.getMessage(), e);
			log.debug(e);
			return;
		} catch (IllegalAccessException e) {
			log.error(e.getMessage(), e);
			log.debug(e);
			return;
		}
		
		//write pid (because after launch we code will be unreachable)
		if(!App.getApp().getPidFile().equals("shell")){
			log.info("Writing pid to file");
			//there is pidFile
			//we do write
			writePid();
			//and registred pid Cleaner as shutdown hook
			PidCleaner cleaner = new PidCleaner();
			Runtime.getRuntime().addShutdownHook(cleaner);
		} else {
			//send output to StdOut
			log.info("Server PID: "+ Env.getMyPid());
		}

	//run
	log.info("Starting server");
	if(context!=null){
		int port = Integer.parseInt(args[0]);
		try {
			WebJetServer.run(context, port);
		} catch (Exception e) {
			log.error("Server failed to start", e);
			log.debug(e);
			return;
		}
	}
				
}
	/**
	 * Provides access to Application settings
	 *  
	 * @return Application Settings object
	 */
	public App app(){
		return App.getApp();
	}
	
	//PRIVATES
	private void init(String[] args){
		//override log level
		App.getApp().setLogLevel(Level.INFO);
		AppLogger log = App.getApp().getLogger();
		//access.log
		String accessLog = System.getProperty("access.log","void");
		if(!accessLog.equals("void")){
			//check on write
			if(Lib.isValidFile(accessLog)){
				//pass value
				App.getApp().setAccessLog(accessLog);
			} else {
				//report
				log.warn("access.log is not writable or doesn't exists. Sending output to StdOut");
				App.getApp().setAccessLog("shell");
			}
		} else {
			//default
			App.getApp().setAccessLog("shell");
		}
		//error.log
		String errorLog = System.getProperty("error.log","void");
		if(!errorLog.equals("void")){
			//check on write
			if(Lib.isValidFile(errorLog)){
				//pass value
				App.getApp().setErrorLog(errorLog);
			} else {
				log.warn("error.log is not writable or doesn't exists. Sending output to StdErr");
				App.getApp().setErrorLog("shell");
			}
		} else {
			//default
			App.getApp().setErrorLog("shell");
		}
		//pidFile
		String pidFile = System.getProperty("pid.file","void");
		if(!pidFile.equals("void")){
			//check on write
			if(Lib.isValidFile(pidFile)){
				//pass value
				App.getApp().setPidFile(pidFile);
			} else {
				log.warn("Pid file is not writable or cannot be created.");
				App.getApp().setPidFile("shell");
			}		
		} else {
			App.getApp().setPidFile("shell");
		}
		//save args
		App.getApp().setArgs(args);
}


private void writePid(){
	AppLogger log = AppLogger.getLogger();
	//if server started successfully, then write pid
	try{
		int pid = Env.getMyPid();
		String pidFile = App.getApp().getPidFile();
		if(!pidFile.equals("shell")){
			//write to file
			Shell.echo(pid,pidFile);
		}	
	}catch(IOException ioe){
		log.error("I cannot write pid file. Please use debug mode to find reason");
	}
}

private static class PidCleaner extends Thread {
	public void run(){
		AppLogger log = AppLogger.getLogger();
		String pidFile = App.getApp().getPidFile();
		try {
			Shell.echo("", pidFile);
		} catch (IOException e) {
			log.debug(e);
			log.info("Could not clean PidFile. Goodbye");
		}
		log.info("PidFile cleaned. Server shutdown.");
	}
}

}