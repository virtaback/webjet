package virta.webjet.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import virta.webjet.logger.WebLogger;
/**
 * Servlet with API 3.0 capability and logging enhancements
 * 
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 * @version 0.5
 *
 */
public class WebJetServlet extends HttpServlet {

	private static final long serialVersionUID = 4351481438512499982L;
	private static HttpServletRequest req;
	private static HttpServletResponse resp;
	
	/**
	 * Sets Request to dispatch it to logger when reply() method called  
	 * 
	 * @param req Unmodified HttpServletRequest
	 */
	protected static void sendRequest(HttpServletRequest req){
		WebJetServlet.req = req;
	}
	/**
	 * Sets Response to dispatch it to logger when reply() method called. Response should be unmodified after calling this method
	 * 
	 * @param resp Response
	 */
	protected static void sendResponse(HttpServletResponse resp){
		WebJetServlet.resp = resp;
	}
	/**
	 * Adds UTF-8 and text/html MIME-type headers to response
	 * Note: calling this method before sendResponse(), causes ServletException
	 * 
	 * @throws ServletException when got not-valid Response or don't get get it at all (normally this happens when useDefaults() called before sendResponse()) 
	 */
	protected static void useDefaults() throws ServletException{
		if(resp!=null){
			resp.setHeader("Content-Type", "text/html; charset=UTF-8");
		} else {
			throw new ServletException("Response is not set");
		}
	} 
	/**
	 * Writes response to Client-side (Browser or other reciever) 
	 * 
	 * @param body Web Page Content to send 
	 * @throws IOException when cannot write to Client-side 
	 * @throws ServletException when got not-valid response or don't get get it at all (normally this happens when sendResponse() wasn't called before reply()) 
	 */
	protected static void reply(String body) throws IOException, ServletException{
		if(resp!=null){
		//reply
		resp.getWriter().println(body);
		
		//logging
		if(req!=null){
		//and log (in future here will be a lot of logic, so just use log.web(req,resp);
		WebLogger logger = WebLogger.getLogger();
		logger.log(req, resp);
		
		}
	} else {
		throw new ServletException("Response is not set");
	}
	}
}
