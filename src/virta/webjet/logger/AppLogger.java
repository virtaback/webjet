package virta.webjet.logger;

import virta.sys.Shell;

/**
 * Application Logger
 *  
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 * @version 0.5
 *
 */
public class AppLogger {
	//TODO timestamp
	
	private int level;
	
	private AppLogger(){
		
		this.level = Level.AppLevel().getLevel();
	}
	
	/**
	 * Getter for Logger. It will always return same object <BR>
	 * Should be access by static way <BR>
	 * Example: AppLogger.getLogger()
	 * 
	 * @return AppLogger object (same all the time)  
	 */
	public static AppLogger getLogger(){
		return new AppLogger();
	}
	//TODO add handlers instead of console. And make parent with common method for App and Web Log + inface 
	
	/**
	 * Writes provided String to StdErr, when Level is DEBUG or ALL
	 * Also tries to return className, methodName and line where method was called
	 * Example: log.debug("Something should be debugged here"); 
	 *  
	 * @param str Message 
	 */
	public void debug(String str){
		if(level==Level.DEBUG || level == Level.ALL){
			
			//trace
			String fullClassName = Thread.currentThread().getStackTrace()[2].getClassName();            
            String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
            String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
            int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
			
            String line = "DEBUG Message: "+str+" ["+className+".java: "+methodName+"() line " +lineNumber + "]";
            
            //TODO to handlers
            Shell.eecho(line);
		}
		
	}
	/**
	 *  Reports exception name, human readable message and stack trace to StdErr, when Level is DEBUG or ALL
	 *  Example: log.debug(e);
	 * 
	 * @param ex
	 */
	public void debug(Throwable ex){
		if(level==Level.DEBUG || level == Level.ALL){
			Shell.eecho("DEBUG Got Exception "+ ex.toString());
			Shell.eecho("Human readable message is: "+ ex.getMessage());
			Shell.eecho("Stack Trace is:");
			ex.printStackTrace(System.err);
		}
	}
	/**
	 * Writes message, className and line to StdOut. Levels TRACE and ALL
	 * 
	 * @param str Message
	 */
	public void trace(String str){
		if(level==Level.TRACE || level == Level.ALL){
			
			String fullClassName = Thread.currentThread().getStackTrace()[2].getClassName();        
			String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
			int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
			
			
			Shell.echo("TRACE: "+str + " ["+ className +":" + lineNumber+ "]");
		}
	}
	/**
	 * Writes info message to StdOut, when level is INFO or above (include ALL)
	 * 
	 * @param str Message
	 */
	public void info(String str){
		if(level>=Level.INFO  || level == Level.ALL){
			Shell.echo("INFO "+str);
		}
	}
	/**
	 * Writes warning to StdErr, when level is WARNING or above + ALL
	 * 
	 * @param str Warning
	 */
	public void warn(String str){
		if(level>=Level.WARNING  || level == Level.ALL){
			Shell.eecho("WARNING "+ str);
		}
	}
	/**
	 * Human-readable warning of exception. Level WARNING or above + ALL
	 *   
	 * @param mess Human-readable Message 
	 * @param ex Exception
	 */
	public void warn(String mess, Throwable ex){
		if(level>=Level.WARNING  || level == Level.ALL){
			Shell.eecho("WARNING "+mess+": "+ex.getClass().getName());
		}
	}
	/**
	 * Displays Error message to StdErr, when level is ERROR or above + ALL
	 * 
	 * @param str Human Readable Message
	 */
	public void error(String str){
		if(level>=Level.ERROR  || level == Level.ALL){
			Shell.eecho("ERROR "+ str);
		}
	}
	/**
	 * Human-readable Error for exception. Level ERROR or above + ALL
	 * @param mess Message for user
	 * @param ex Exception
	 */
	public void error(String mess, Throwable ex){
		if(level>=Level.ERROR  || level == Level.ALL){
			Shell.eecho("ERROR "+mess+": "+ex.getClass().getName());
		}
	}
	
}
