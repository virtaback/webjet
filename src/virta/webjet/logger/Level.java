package virta.webjet.logger;
/**
 * Defines List of Log Levels.
 * Methods of this class normally shouldn't be use directly outside webjet.jar 
 *  
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 * @version 0.5
 *
 */
public class Level {
	
	//predefined levels
	/**
	 * No messages will be displayed
	 */
	public static final int NONE = 0;
	/**
	 * Only error messages will be displayed
	 */
	public static final int ERROR = 5;
	/**
	 * Warnings will be displayed
	 */
	public static final int WARNING = 7;
	/**
	 * Only info messages will be displayed
	 */
	public static final int INFO = 10;
	/**
	 * Trace messages will be displayed
	 */
	public static final int TRACE = 70;
	/**
	 * Debug (+info, error) messages will be displayed
	 */
	public static final int DEBUG = 100;
	/**
	 * All messages will be displayed
	 */
	public static final int ALL = 100000;
	
	private static Level self;
	
	private int level;
	
	private Level(){
		//default level
		//this.level = Level.INFO;
	}
	/**
	 * Returns Level object
	 * Normally should be used inside WebJet
	 * @return Level object (same one everytime)
	 */
	public static Level AppLevel(){
		if(self==null){
			self = new Level(); 
		}
		return self;
	} 
	/**
	 * Sets new Log Level
	 *  
	 * @param level Level as valid int
	 */
	public void setLevel(int level){
		this.level = level;
	}
	/**
	 * Return current log level
	 * 
	 * @return current log level
	 */
	public int getLevel(){
		return level;
	}
	/**
	 * Returns integer for current log level
	 * 
	 * @return current level as int 
	 */
	public int toInt(){
		return this.level;
	}
	
	
}
