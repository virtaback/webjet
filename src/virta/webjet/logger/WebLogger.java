package virta.webjet.logger;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import virta.sys.Shell;
import virta.webjet.util.App;
/**
 * Defines logging for WebJetServlet. Access.log and Error.log
 * Currently cannot be extended (for custom logging)
 *  
 * @author Alex Muravya (aka kyberorg) <asm@virtalab.net>
 * @version 0.5
 *
 */
public final class WebLogger {
	
	private static WebLogger self;
	private WebLogger(){}
	/**
	 * Returns Logger object
	 * In normal case shouldn't be used
	 * @return WebLogger
	 */
	public static WebLogger getLogger(){
		if(self==null){
			self = new WebLogger();
		}
		return self;
	}

	/**
	 * Log entry point (dispatcher). Logs every request
	 * 
	 * @param req Log-ready Request
	 * @param resp Log-ready Responce 
	 * @throws IOException when cannot write into requested (from params) resource
	 */
	public void log(HttpServletRequest req,HttpServletResponse resp) throws IOException{
		//TODO more complex logging logic
		if(resp.getStatus()>=400){
			this.errorLog(req, resp);
		} 
		this.accessLog(req, resp);
	}
	
	private void accessLog(HttpServletRequest req,HttpServletResponse resp) throws IOException{
		String log = this.StringPrepapator(req, resp);
		String dest = App.getApp().getAccessLog();
		if(!dest.equals("shell")){
			Shell.echo(log, dest,true);
		} else {
			//to StdOut
			Shell.echo(log);
		}
		
		
	}
	private void errorLog(HttpServletRequest req,HttpServletResponse resp) throws IOException{
		String log = this.StringPrepapator(req, resp);
		String dest = App.getApp().getErrorLog();
		if(!dest.equals("shell")){
			Shell.echo(log, dest,true);
		} else {
			//to StdErr
			Shell.eecho(log);
		}
	}
	
	private String StringPrepapator(HttpServletRequest req,HttpServletResponse resp){
		  //1) make log String from request
        String remoteIp = req.getRemoteHost();
        //time
        Date now = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy hh:mm:ss");
        StringBuffer sb = new StringBuffer();
        String ts = formatter.format(now);
        
       //method
        String method = req.getMethod();
        //status
        int status = resp.getStatus();
        //uri
        String url = req.getRequestURL().toString();
        String uri = req.getQueryString(); 
        //User-Agent
        String userAgent = req.getHeader("User-Agent");
        
        sb.append(remoteIp);
        sb.append(" - [");
        sb.append(ts);
        sb.append("] - ");
        sb.append(method);
        sb.append(" - ");
        sb.append(status);
        sb.append(" - ");
        sb.append(url);
        if(uri!=null){
        	sb.append("?");
        	sb.append(uri);
        }
        sb.append(" - ");
        sb.append(userAgent);
        
        String line = sb.toString();
        
        //String line = remoteIp + " - [" + ts + "] - " +  method + " - " + status + " - " + url+"?"+uri + " - " + userAgent;
        return line;
	}
}
