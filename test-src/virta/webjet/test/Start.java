package virta.webjet.test;

import virta.webjet.logger.Level;
import virta.webjet.server.Server;

public class Start {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		//creating server
		Server server = new Server(args);
		
		//Rewrite defaults (optional)		
		server.app().setLogLevel(Level.DEBUG);

		//add Handler
		server.setHandler("/","virta.webjet.test");
		//addServlet
		server.addServlet("RootServlet","/");
		server.addServlet("DebugServlet","/debug");
		server.addServlet("LangServlet","/lang");
		//start Server
		server.start();
	}

}
